package com.softdesign.mvpauth.mvp.presenters;

public interface IAuthPresenter {

    void initView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();
    void setCallRootActivity(boolean callRootActivity);
    boolean getCallRootActivity();
}
