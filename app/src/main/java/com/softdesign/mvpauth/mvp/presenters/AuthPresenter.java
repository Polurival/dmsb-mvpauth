package com.softdesign.mvpauth.mvp.presenters;

import android.os.Handler;

import com.softdesign.mvpauth.mvp.models.AuthModel;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

public class AuthPresenter extends AbstractPresenter<IAuthView>
        implements IAuthPresenter {

    private static final AuthPresenter sPresenter = new AuthPresenter();
    private final AuthModel mAuthModel;
    private boolean mCallRootActivity;

    private AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return sPresenter;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                String userEmail = getView().getAuthPanel().getUserEmail();
                String userPassword = getView().getAuthPanel().getUserPassword();

                if (!userEmail.isEmpty() && !userPassword.isEmpty()) {
                    getView().showLoad();
                    runWithDelay();
                    mAuthModel.loginUser(userEmail, userPassword);
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            getView().showCatalogScreen();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isUserAuth();
    }

    @Override
    public void setCallRootActivity(boolean callRootActivity) {
        mCallRootActivity = callRootActivity;
    }

    @Override
    public boolean getCallRootActivity() {
        return mCallRootActivity;
    }

    private void runWithDelay(){
        final Handler handler = new Handler();
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (getView() != null) {
                            getView().hideLoad();
                            if (mAuthModel.isUserAuth()) {
                                if (mCallRootActivity) {
                                    getView().showCatalogScreen();
                                } else {
                                    getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                                    getView().hideLoginBtn();
                                }
                            } else {
                                getView().showMessage("Ошибка авторизации");
                            }
                        }
                    }
                }, 3000
        );
    }
}
