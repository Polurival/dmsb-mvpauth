package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.mvp.models.ProductModel;
import com.softdesign.mvpauth.mvp.views.IProductView;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

public class ProductPresenter extends AbstractPresenter<IProductView>
        implements IProductPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(ProductPresenter.class.getName());

    private ProductModel mModel;
    private ProductDto mProduct;

    private static ProductPresenter newInstance(ProductDto product) {
        return new ProductPresenter(product);
    }

    private ProductPresenter(ProductDto product) {
        mModel = new ProductModel();
        mProduct = product;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
        }
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }
}
