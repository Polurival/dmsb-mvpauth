package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.mvp.models.CatalogModel;
import com.softdesign.mvpauth.mvp.views.ICatalogView;

import java.util.List;

public class CatalogPresenter extends AbstractPresenter<ICatalogView>
        implements ICatalogPresenter {

    private static final CatalogPresenter sPresenter = new CatalogPresenter();
    private CatalogModel mModel;
    private List<ProductDto> mProductList;
    private int mRootProductCount = 0;

    private CatalogPresenter() {
        mModel = new CatalogModel();
    }

    public static CatalogPresenter getInstance() {
        return sPresenter;
    }

    @Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }
    }

    @Override
    public void clickOnByBtn(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                addProductToCard(position);
                getView().updateRootProductCounter(mRootProductCount);
                getView().showAddToCartMessage(mProductList.get(position));
            } else {
                getView().showAuthScreen();
            }
        }

    }

    @Override
    public boolean checkUserAuth() {
        return mModel.checkUserAuth();
    }

    @Override
    public void addProductToCard(int position) {
        mRootProductCount += mProductList.get(position).getCount();
    }
}
