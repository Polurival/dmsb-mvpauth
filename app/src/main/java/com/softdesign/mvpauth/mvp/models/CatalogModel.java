package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.DataManager;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;

import java.util.List;

public class CatalogModel {

    private DataManager mDataManager = DataManager.getInstance();

    public CatalogModel() {

    }

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean checkUserAuth() {
        return !mDataManager.getToken().isEmpty();
    }
}
