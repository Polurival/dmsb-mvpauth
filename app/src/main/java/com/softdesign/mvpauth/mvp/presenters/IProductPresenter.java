package com.softdesign.mvpauth.mvp.presenters;

interface IProductPresenter {

    void clickOnPlus();
    void clickOnMinus();
}
