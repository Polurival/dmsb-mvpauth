package com.softdesign.mvpauth.mvp.presenters;

interface ICatalogPresenter {
    void clickOnByBtn(int position);
    boolean checkUserAuth();
    void addProductToCard(int position);
}
