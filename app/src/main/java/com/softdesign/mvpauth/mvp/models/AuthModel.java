package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.DataManager;

public class AuthModel {

    private DataManager mDataManager;
    
    public AuthModel() {
        mDataManager = DataManager.getInstance();
    }
    
    public boolean isUserAuth() {
        return !mDataManager.getToken().isEmpty();
    }
    
    public void loginUser(String email, String password) {
        if (!email.isEmpty() && !password.isEmpty()) {
            mDataManager.saveToken(email + " " + password);
        }
    }
}
