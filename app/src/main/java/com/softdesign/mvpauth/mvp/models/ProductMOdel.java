package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.DataManager;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;

public class ProductModel {
    private final DataManager mDataManager = DataManager.getInstance();

    public ProductDto getProductById(int productId) {
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
