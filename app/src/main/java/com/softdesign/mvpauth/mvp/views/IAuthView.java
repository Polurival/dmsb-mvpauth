package com.softdesign.mvpauth.mvp.views;

import com.android.annotations.Nullable;
import com.softdesign.mvpauth.mvp.presenters.IAuthPresenter;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

public interface IAuthView extends IView {

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    @Nullable
    AuthPanel getAuthPanel();

    void showCatalogScreen();
}
