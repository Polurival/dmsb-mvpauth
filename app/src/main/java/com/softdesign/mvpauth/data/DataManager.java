package com.softdesign.mvpauth.data;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import com.softdesign.mvpauth.App;
import com.softdesign.mvpauth.data.network.PicassoCache;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DataManager {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(DataManager.class.getName());
    private static DataManager sInstance = null;

    private final Context mContext;
    private final PreferencesManager mPreferencesManager;
    private final Picasso mPicasso;

    private List<ProductDto> mMockProductList;

    private DataManager() {
        Log.d(TAG, "DataManager create");

        mContext = App.getContext();
        mPreferencesManager = new PreferencesManager();
        mPicasso = new PicassoCache(mContext).getPicassoInstance();

        // TODO: 29.10.2016 this is temp
        generateMockData();
    }

    public static DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }

    public Context getContext() {
        return mContext;
    }

    public Picasso getPicasso() {
        return mPicasso;
    }

    // region ========== Shared Preferences ========================================================

    public void saveToken(String token) {
        mPreferencesManager.saveToken(token);
    }

    public String getToken() {
        return mPreferencesManager.getToken();
    }

    public void saveUserName(String userName) {
        mPreferencesManager.saveUserName(userName);
    }

    public String getUserName() {
        return  mPreferencesManager.getUserName();
    }

    // endregion

    // region ========== Network ===================================================================

    public void loadImagePicasso(final String uri, final ImageView toImage, final Drawable dummy) {
        mPicasso
                .load(uri).fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(dummy)
                .error(dummy)
                .into(toImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Picasso load image from cache");
                    }

                    @Override
                    public void onError() {
                        DataManager.getInstance().getPicasso()
                                .load(uri)
                                .fit()
                                .centerCrop()
                                .placeholder(dummy)
                                .error(dummy)
                                .into(toImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d(TAG, "Picasso load image from network");
                                    }

                                    @Override
                                    public void onError() {
                                        Log.d(TAG, "Picasso could not fetch photo from network ");
                                    }
                                });
                    }
                });
    }

    // endregion

    public ProductDto getProductById(int productId) {
        // TODO: 28.10.2016 this is temp
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 28.10.2016 create method
    }

    private void generateMockData() {
        // TODO: 29.10.2016 this is temp
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Riga-120B", "https://pp.vk.me/c631227/v631227752/20b53/xJlf5YYr39Q.jpg", "Кассетная магнитола", 100, 1));
        mMockProductList.add(new ProductDto(2, "Маяк-205", "https://pp.vk.me/c631227/v631227752/20b41/_3MJfl60IiM.jpg", "Катушечный магнитофон", 200, 1));
        mMockProductList.add(new ProductDto(3, "Иж-303", "https://pp.vk.me/c627525/v627525737/4013b/BRJL29dHfEQ.jpg", "Кассетный магнитофон", 300, 1));
        mMockProductList.add(new ProductDto(4, "Океан-209", "https://pp.vk.me/c627525/v627525737/40129/6LcyXS86dpE.jpg", "Радиоприемник", 400, 1));
        mMockProductList.add(new ProductDto(5, "Маяк-001 Стерео", "https://pp.vk.me/c633630/v633630486/18775/aiZwadWQx64.jpg", "Катушечный магнитофон высшего класса", 500, 1));
        mMockProductList.add(new ProductDto(6, "Вега МП-122С", "https://pp.vk.me/c628417/v628417283/1d919/I4pPzyKTctQ.jpg", "Стерефонический двухкассетный магнитофон\nВнешний усилитель Вега 50У-122С", 600, 1));
        mMockProductList.add(new ProductDto(7, "Комета-225", "https://pp.vk.me/c410427/v410427205/5589/yWEbdr-55Ak.jpg", "Однокассетный стерефонический магнитофон с акустическими системами", 700, 1));
    }

    public List<ProductDto> getProductList() {
        // TODO: 29.10.2016 this is temp
        return mMockProductList;
    }
}
