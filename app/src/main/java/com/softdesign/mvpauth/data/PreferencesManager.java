package com.softdesign.mvpauth.data;

import android.content.SharedPreferences;

import com.softdesign.mvpauth.App;
import com.softdesign.mvpauth.utils.ConstantManager;

class PreferencesManager {

    private final SharedPreferences mSharedPreferences;

    PreferencesManager() {
        mSharedPreferences = App.getSharedPreferences();
    }

    void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.TOKEN_KEY, token);
        editor.apply();
    }

    String getToken() {
        return mSharedPreferences.getString(ConstantManager.TOKEN_KEY, "");
    }

    void saveUserName(String userName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_NAME_KEY, userName);
        editor.apply();
    }

    String getUserName() {
        return mSharedPreferences.getString(ConstantManager.USER_NAME_KEY, "");
    }
}
