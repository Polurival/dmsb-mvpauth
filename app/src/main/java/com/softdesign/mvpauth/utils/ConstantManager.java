package com.softdesign.mvpauth.utils;

public interface ConstantManager {

    String TAG_PREFIX = "MVP ";

    String FONT_BOOK = "PTBebasNeueBook";
    String FONT_REGULAR = "PTBebasNeueRegular";
    String FONT_COMIC = "comic";

    String TOKEN_KEY = "TOKEN_KEY";
    String USER_NAME_KEY = "USER_NAME_KEY";

    //String PATTERN_PASSWORD = "^\\\\S{8,}$";
    String PATTERN_EMAIL = "\\w+@\\w+.\\w{2,}";
}
