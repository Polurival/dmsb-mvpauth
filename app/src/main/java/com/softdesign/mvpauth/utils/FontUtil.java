package com.softdesign.mvpauth.utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.TextView;

public class FontUtil {

    private static Typeface sBookFont, sRegilarFont, sComicFont;

    public static void initFont(Context context) {

        sBookFont = Typeface.createFromAsset(context.getAssets(),
                "fonts/" + ConstantManager.FONT_BOOK + ".ttf");
        sRegilarFont = Typeface.createFromAsset(context.getAssets(),
                "fonts/" + ConstantManager.FONT_REGULAR + ".ttf");
        sComicFont = Typeface.createFromAsset(context.getAssets(),
                "fonts/" + ConstantManager.FONT_COMIC + ".ttf");
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        Typeface typeface = getFont(fontName);
        if (typeface != null) {
            textView.setTypeface(typeface);
        }
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(Button button, String fontName) {
        Typeface typeface = getFont(fontName);
        if (typeface != null) {
            button.setTypeface(typeface);
        }
    }

    private static Typeface getFont(String fontName) {
        switch (fontName) {
            case ConstantManager.FONT_BOOK:
                return sBookFont;
            case ConstantManager.FONT_REGULAR:
                return sRegilarFont;
            case ConstantManager.FONT_COMIC:
                return sComicFont;
            default:
                return null;
        }
    }
}
