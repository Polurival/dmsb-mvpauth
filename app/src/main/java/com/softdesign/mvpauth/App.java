package com.softdesign.mvpauth;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.softdesign.mvpauth.utils.FontUtil;

public class App extends Application {

    private static Context sContext;
    private static SharedPreferences sSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        FontUtil.initFont(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }
}
