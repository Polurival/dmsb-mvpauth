package com.softdesign.mvpauth.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.DataManager;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.databinding.FragmentProductBinding;
import com.softdesign.mvpauth.mvp.presenters.ProductPresenter;
import com.softdesign.mvpauth.mvp.presenters.ProductPresenterFactory;
import com.softdesign.mvpauth.mvp.views.IProductView;
import com.softdesign.mvpauth.ui.activities.RootActivity;

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {

    private FragmentProductBinding mBinding;
    private ProductPresenter mPresenter;

    public ProductFragment() {

    }

    public static ProductFragment newIsnance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDto product = bundle.getParcelable("PRODUCT");
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mBinding.plusBtn.setOnClickListener(this);
        mBinding.minusBtn.setOnClickListener(this);
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }
    }

    // region =============================== IProductView =========================================

    @Override
    public void showProductView(ProductDto product) {
        mBinding.productNameTxt.setText(product.getProductName());
        mBinding.productDescriptionTxt.setText(product.getDescription());
        updateProductCountView(product);
        DataManager.getInstance().loadImagePicasso(
                product.getImageUrl(),
                mBinding.productImage,
                mBinding.productImage.getContext().getResources().getDrawable(R.drawable.img_bg)
        );

    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mBinding.productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mBinding.productPriceTxt.setText(
                    String.valueOf(product.getCount() * product.getPrice()) + ".-");
        } else {
            mBinding.productPriceTxt.setText(
                    String.valueOf(product.getPrice()) + ".-");
        }
    }

    // endregion

    // region ==================================== IView ===========================================

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    // endregion
}
