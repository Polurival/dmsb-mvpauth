package com.softdesign.mvpauth.ui.activities;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.mvpauth.BuildConfig;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.DataManager;
import com.softdesign.mvpauth.databinding.ActivityRootBinding;
import com.softdesign.mvpauth.mvp.views.IView;
import com.softdesign.mvpauth.ui.fragments.AuthFragment;
import com.softdesign.mvpauth.ui.fragments.CatalogFragment;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;
import com.softdesign.mvpauth.utils.TransformRoundedImage;

public class RootActivity extends BaseActivity
        implements IView, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(RootActivity.class.getName());
    private ActivityRootBinding mBinding;
    private DataManager mDataManager;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_root);
        mDataManager = DataManager.getInstance();

        initToolBar();
        initDarwer();

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    private void initDarwer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                mBinding.drawerLayout,
                mBinding.toolbar,
                R.string.open_drawer,
                R.string.close_drawer);
        mBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mBinding.navView.setNavigationItemSelectedListener(this);

        loadUserAvatar();
        setUseName();
    }

    private void loadUserAvatar() {
        ImageView userAvatar = (ImageView) mBinding.navView.getHeaderView(0).findViewById(R.id.user_avatar);

        if (!mDataManager.getToken().isEmpty()) {
            mDataManager.getPicasso()
                    .load("file:///android_asset/16w.png")
                    .transform(new TransformRoundedImage())
                    .into(userAvatar);
        } else {
            mDataManager.getPicasso()
                    .load(R.drawable.ic_account_circle_24dp)
                    .into(userAvatar);
        }
    }

    private void setUseName() {

        if (!mDataManager.getToken().isEmpty()) {
            TextView userName = (TextView) mBinding.navView.getHeaderView(0).findViewById(R.id.user_name);
            userName.setText(mDataManager.getUserName());
        }
    }

    private void initToolBar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void updateProductCounter(int productCount) {
        mBinding.productCount.setText(String.valueOf(productCount));
    }

    public void showAuthScreen() {
        Fragment fragment = new AuthFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("CALL_ROOT_ACTIVITY", true);
        fragment.setArguments(bundle);
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void returnToCatalog() {
        Fragment fragment = new CatalogFragment();
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_account:
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }

        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (mFragmentManager.getBackStackEntryCount() > 0) {
                super.onBackPressed();
            } else {
                final AlertDialog dialogBuilder =
                        new AlertDialog.Builder(this, R.style.AppDialog)
                                .setMessage(R.string.confirm_exit)
                                .setCancelable(true)
                                .setPositiveButton(R.string.yes_caption, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                })
                                .setNegativeButton(R.string.no_caption, null)
                                .create();
                dialogBuilder.show();
            }
        }
    }

    // region ==================================== IView ===========================================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mBinding.coordinatorContainer, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            // TODO: 20.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    // endregion
}
