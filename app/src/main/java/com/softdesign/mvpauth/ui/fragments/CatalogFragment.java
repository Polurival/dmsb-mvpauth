package com.softdesign.mvpauth.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.databinding.FragmentCatalogBinding;
import com.softdesign.mvpauth.mvp.presenters.CatalogPresenter;
import com.softdesign.mvpauth.mvp.views.ICatalogView;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.adapters.CatalogAdapter;

import java.util.List;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    private FragmentCatalogBinding mBinding;
    private CatalogPresenter mPresenter = CatalogPresenter.getInstance();

    public CatalogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_catalog, container, false);
        mPresenter.takeView(this);
        mPresenter.initView();

        mBinding.addToCardBtn.setOnClickListener(this);

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_to_card_btn:
                mPresenter.clickOnByBtn(mBinding.productPager.getCurrentItem());
                break;
        }
    }

    // region =============================== ICatalogView =========================================

    @Override
    public void showAddToCartMessage(ProductDto product) {
        showMessage("Товар " + product.getProductName() + "успешно добавлен в корзину");
    }

    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productList) {
            adapter.addItem(product);
        }
        mBinding.productPager.setAdapter(adapter);
        mBinding.pagerIndicator.setViewPager(mBinding.productPager);
    }

    @Override
    public void showAuthScreen() {
        getRootActivity().showAuthScreen();
    }

    @Override
    public void updateRootProductCounter(int productCount) {
        getRootActivity().updateProductCounter(productCount);
    }

    // endregion

    // region ================================== IView =============================================

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    // endregion
}
