package com.softdesign.mvpauth.ui.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.databinding.FragmentAuthBinding;
import com.softdesign.mvpauth.mvp.presenters.AuthPresenter;
import com.softdesign.mvpauth.mvp.presenters.IAuthPresenter;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.activities.AuthActivity;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

public class AuthFragment extends Fragment implements IAuthView, View.OnClickListener {

    private FragmentAuthBinding mBinding;
    private final AuthPresenter mPresenter = AuthPresenter.getInstance();


    public AuthFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);
        mPresenter.takeView(this);
        mPresenter.initView();

        mBinding.loginBtn.setOnClickListener(this);
        mBinding.showCatalogBtn.setOnClickListener(this);

        readBundle(getArguments());
        if (mPresenter.getCallRootActivity()) {
            mBinding.showCatalogBtn.setText(getString(R.string.return_to_catalog));
        }
        return mBinding.getRoot();
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mPresenter.setCallRootActivity(bundle.getBoolean("CALL_ROOT_ACTIVITY", false));
        }
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.social_vk_btn:
                mPresenter.clickOnVk();
                break;
            case R.id.social_twitter_btn:
                mPresenter.clickOnTwitter();
                break;
            case R.id.social_fb_btn:
                mPresenter.clickOnFb();
                break;
        }
    }

    // region =========== IAuthView ================================================================

    @Override
    public IAuthPresenter getPresenter() {
        return null;
    }

    @Override
    public void showLoginBtn() {
        mBinding.loginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mBinding.loginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mBinding.authWrapper;
    }

    @Override
    public void showCatalogScreen() {
        if (getActivity() instanceof AuthActivity) {
            Intent intent = new Intent(getActivity(), RootActivity.class);
            startActivity(intent);
            getActivity().finish();
        } else
        if (getActivity() instanceof RootActivity) {
            getRootActivity().returnToCatalog();
        }
    }

    // endregion

    // region ========== IView =====================================================================

    @Override
    public void showMessage(String message) {
        if (getRootActivity() != null) {
            getRootActivity().showMessage(message);
        } else
        if (getAuthActivity() != null) {
            getAuthActivity().showMessage(message);
        }
    }

    @Override
    public void showError(Throwable e) {
        if (getRootActivity() != null) {
            getRootActivity().showError(e);
        } else
        if (getAuthActivity() != null) {
            getAuthActivity().showError(e);
        }
    }

    @Override
    public void showLoad() {
        if (getRootActivity() != null) {
            getRootActivity().showLoad();
        } else
        if (getAuthActivity() != null) {
            getAuthActivity().showLoad();
        }
    }

    @Override
    public void hideLoad() {
        if (getRootActivity() != null) {
            getRootActivity().hideLoad();
        } else
        if (getAuthActivity() != null) {
            getAuthActivity().hideLoad();
        }
    }

    private RootActivity getRootActivity() {
        if (getActivity() instanceof RootActivity) {
            return (RootActivity) getActivity();
        }
        return null;
    }

    private AuthActivity getAuthActivity() {
        if (getActivity() instanceof AuthActivity) {
            return (AuthActivity) getActivity();
        }
        return null;
    }

    // endregion
}
