package com.softdesign.mvpauth.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.softdesign.mvpauth.BuildConfig;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.mvp.views.IView;
import com.softdesign.mvpauth.ui.fragments.AuthFragment;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

public class AuthActivity extends BaseActivity implements IView {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(AuthActivity.class.getName());
    private FragmentManager mFragmentManager;

    // region ===================== Life cycle ==========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new AuthFragment())
                    .commit();
        }
    }

    // endregion

    // region ==================== IView ===========================================================

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            // TODO: 20.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    // endregion
}

